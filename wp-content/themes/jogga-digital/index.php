<?php 
	get_header(); 
	//Template name: Home
?>

	  <!-- linha um  -->
	  <section class="line-one">
	    <div class="container">

	      <h2 class="title">Título da seção</h2>
	      <h3 class="subtitle">Subtitulo da seção</h3>

	      <ul class="line-one__list row">

	        <li class="col-sm-3">
	          <div class="line-one__item">
	            <img src="<?php echo get_template_directory_uri();?>/dist/images/svg/phone.svg" alt="" class="line-one__icon">
	            <div class="line-one__caption">
	              <strong>Lorem Ipsum</strong>
	              <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
	            </div>
	          </div>
	        </li>

	        <li class="col-sm-3">
	          <div class="line-one__item">
	            <img src="<?php echo get_template_directory_uri();?>/dist/images/svg/phone.svg" alt="" class="line-one__icon">
	            <div class="line-one__caption">
	              <strong>Lorem Ipsum</strong>
	              <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
	            </div>
	          </div>
	        </li>

	        <li class="col-sm-3">
	          <div class="line-one__item">
	            <img src="<?php echo get_template_directory_uri();?>/dist/images/svg/phone.svg" alt="" class="line-one__icon">
	            <div class="line-one__caption">
	              <strong>Lorem Ipsum</strong>
	              <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
	            </div>
	          </div>
	        </li>

	        <li class="col-sm-3">
	          <div class="line-one__item">
	            <img src="<?php echo get_template_directory_uri();?>/dist/images/svg/phone.svg" alt="" class="line-one__icon">
	            <div class="line-one__caption">
	              <strong>Lorem Ipsum</strong>
	              <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
	            </div>
	          </div>
	        </li>

	      </ul>

	    </div>
	  </section>

	  <!-- linha dois -->
	  <section class="line-two">
	    <div class="container">

	      <h2 class="title">Título da seção</h2>
	      <h3 class="subtitle">Subtitulo da seção</h3>

	      <ul class="line-two__list row">

	        <li class="col-sm-6">
	          <div class="line-two__item">
	            <figure class="line-two__image">
	              <img src="http://via.placeholder.com/600x600" alt="Imagem contem uma foto">
	            </figure>
	          </div>
	        </li>

	        <li class="col-sm-6">
	          <div class="line-two__item">
	            <figure class="line-two__image">
	              <img src="http://via.placeholder.com/600x600" alt="Imagem contem uma foto">
	            </figure>
	          </div>
	        </li>

	      </ul>

	    </div>
	  </section>

	  <!-- linha três -->
	  <section class="line-three">
	    <div class="container">

	      <h2 class="title">Título da seção</h2>
	      <h3 class="subtitle">Subtitulo da seção</h3>

	      <div class="row">
	        <div class="col-sm-6">
	          <figure class="line-three__image">
	            <img src="http://via.placeholder.com/600x350" alt="Imagem contem uma foto">
	          </figure>
	        </div>

	        <div class="col-sm-6">
	          <h4 class="line-three__caption">Lorem Ipsum</h4>
	          <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam sint eos quibusdam reiciendis quas dolor saepe
	            vitae, dolorum aut ea voluptatum, eligendi accusantium sequi vel nihil quisquam! Nam, rerum, ipsam.</p>
	          <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam sint eos quibusdam reiciendis quas dolor saepe
	            vitae, dolorum aut ea voluptatum, eligendi accusantium sequi vel nihil quisquam! Nam, rerum, ipsam.</p>
	        </div>
	      </div>

	    </div>
	  </section>

	  <!-- localização -->
	  <section class="location">
	    <div class="col-md-5">
	      <address class="location__address">
	        <h3 class="location__head">Nome do lugar</h3>
	        <strong class="location__place">Estamos localizados na</strong>
	         <p class="location__text">
	          Lorem ipsum dolor sit amet n.999 <br>
	          Lorem ipsum
	         </p>
	      </address>
	    </div>
	    <div class="col-md-7 none">
	      <div class="location__mapa" id="mapa"></div>
	    </div>  
	  </section>

	</main>


<?php get_footer(); ?>
