<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) {
		  echo ' :';
	  } ?><?php bloginfo( 'name' ); ?></title>

  <link href="//www.google-analytics.com" rel="dns-prefetch">
  <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="shortcut icon">
  <link href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" rel="apple-touch-icon-precomposed">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php bloginfo( 'description' ); ?>">

	<?php wp_head(); ?>
  

</head>
<body <?php body_class(); ?>>
<!-- wrapper -->

  <!-- cabeçalho -->
  <header class="header">
    <div class="container">

      <h1 class="header__logo">Nome do site</h1>

      <a href="tel:" class="header__phone">
        <svg class="icon--phone">
          <use xlink:href="<?php echo get_template_directory_uri();?>/dist/images/svg/svg.svg#phone"></use>
        </svg>
        <span class="caption">Ligue agora:</span>
        <span class="number">81 9999-9999</span>
        <svg class="icon--arrow">
          <use xlink:href="<?php echo get_template_directory_uri();?>/dist/images/svg/svg.svg#arrow"></use>
        </svg>
      </a>

    </div>
  </header>

  <main class="content">

    <!-- destaque -->
    <section class="featured">      

      <div class="container">
        <form action="#" method="post" class="featured__form" id="form">
          <fieldset class="row">
            <div class="col-sm-12">
              <legend class="head">Título do formulário</legend>
            </div>            
            <div class="col-sm-4 col-md-12">
              <input type="text" name="nome" id="nome" placeholder="Seu nome" class="form-control required">
            </div>
            <div class="col-sm-4 col-md-12">
              <input type="email" name="email" id="email" placeholder="Seu email" class="form-control required">
            </div>
            <div class="col-sm-4 col-md-12">
              <input type="tel" name="telefone" id="telefone" minlength="14" placeholder="Seu telefone" class="form-control telefone required">
            </div>
            <div class="col-sm-12">
              <button type="submit" class="btn">Enviar</button>
            </div>
          </fieldset>
        </form>
      </div>
    </section>
