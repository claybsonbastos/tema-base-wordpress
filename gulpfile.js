var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    htmlmin = require('gulp-htmlmin'),
    cleanCSS = require('gulp-clean-css'),
    svgstore = require('gulp-svgstore'),
    svgmin = require('gulp-svgmin'),
    myip = require('quick-local-ip'),
    connect = require('gulp-connect'),
    clean = require('gulp-clean'),
    path = require('path'),
    imagemin = require('gulp-imagemin');
    browserSync = require('browser-sync');

function swallowError(error) {
    console.log(error.toString());
    this.emit('end');
}

// LIMPANDO SVG
gulp.task('clean-svg', function () {
    return gulp.src('wp-content/themes/jogga-digital/assets/imagens/svg/svg.svg', { read: false })
        .pipe(clean());
});

// GERANDO SVG
gulp.task('svg', ['clean-svg'], function () {
    return gulp.src('wp-content/themes/jogga-digital/assets/imagens/svg/**/*.svg')
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + '-',
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest('wp-content/themes/jogga-digital/dist/imagens/svg'))
});

// OTIMIZANDO IMAGENS
gulp.task('images', function () {
    return gulp.src([
        'wp-content/themes/jogga-digital/assets/imagens/**/*.gif',
        'wp-content/themes/jogga-digital/assets/imagens/**/*.jpg',
        'wp-content/themes/jogga-digital/assets/imagens/**/*.png',
        'wp-content/themes/jogga-digital/assets/imagens/**/*.svg'
    ])
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.jpegtran({ progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({ plugins: [{ removeViewBox: true }] })
        ]))
        .pipe(gulp.dest('wp-content/themes/jogga-digital/dist/imagens/'))
});


// OTIMIZAR OS SCRIPTS
var global = [
    'wp-content/themes/jogga-digital/assets/js/plugins/jquery.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/maskedinput.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/modernizr.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/owl.carousel.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/placeholder.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/validate.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/waypoints.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/svg4everybody.js',
    'wp-content/themes/jogga-digital/assets/js/plugins/fancybox.js',
    'wp-content/themes/jogga-digital/assets/js/modules/maps.js',
    'wp-content/themes/jogga-digital/assets/js/modules/page.js'
];

gulp.task('scripts', function () {
    gulp.src(global)
        .pipe(concat("scripts.js"))
        .pipe(uglify())
        .pipe(gulp.dest('wp-content/themes/jogga-digital/dist/js'));
});

// MINIFICAR CSS
gulp.task('less', function () {
    gulp.src('wp-content/themes/jogga-digital/assets/less/main.less')
        .pipe(less())
        .on('error', swallowError)
        .pipe(cleanCSS())
        .pipe(gulp.dest('wp-content/themes/jogga-digital/dist/css'))
        .pipe(connect.reload());
});

// SERVIDOR
gulp.task('browserSync', function() {
  //watch files
    var files = [
    './wp-content/themes/jogga-digital/assets/less/**/*.less',
    './wp-content/themes/jogga-digital/*.php',
    './wp-content/themes/jogga-digital/assets/js/**/*.js'
    ];

  browserSync.init(files,{
      proxy: "localhost/tema-base-wordpress/"
  }); 
});


// WATCH LESS, SCRIPTS E LIVERELOAD
gulp.task('watch', function () {
    gulp.watch("wp-content/themes/jogga-digital/*.php");
    gulp.watch('wp-content/themes/jogga-digital/assets/less/**/*.less', ['less']);
});

gulp.task('default', ['less', 'scripts', 'images', 'svg', 'browserSync', 'watch']);