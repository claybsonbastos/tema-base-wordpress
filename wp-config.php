<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'db_tema_base');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}S9@-@5J=lG?b+<Ff9_]c}Ol7_^9Z>(AV;zJZc:DFv)_}+jw<izKoa1+V5/Lih8.');
define('SECURE_AUTH_KEY',  'l~r^/CZ{!vrYM-8k9yvX2H9n/#C{qu[+[lZ0GmNp.6d{q/;<>9(RGwFg71I[Y2<-');
define('LOGGED_IN_KEY',    'i{mL|#26CF,|I0ukbZ K8{P@/+-4*1HDI,~>-wKa&]De+8n|B|! *=c]{UD,|3zR');
define('NONCE_KEY',        'Y$6N>Q$RX5H7H[me=E.H1QSnH}-]?P/)EO=aK`U 9_@eJ1+797Z+H3NdP]nh/aAp');
define('AUTH_SALT',        'fBmfI;^9YPR?Fx}olPJz 6_>93j>$HTPx3fC-I+rVAr&:ll?uwi(+~i2q7*xNl=}');
define('SECURE_AUTH_SALT', 'AC:`vx$8@dhR:h(V_Wb}^<JG*-6I-7?{xBpwU>kyQRx%EJR&{ED6fj8lQUdo-S][');
define('LOGGED_IN_SALT',   'Rq-}@$Z2KnUH}`)ZP)g|+B,y2e?-uFCf?mhheDraJ@>Z8|MJ4c|F~Px`u3uw-W_*');
define('NONCE_SALT',       ';tlFJaSSoV<? ={vLegPr/09+T=MfXT]Zth-|=Y76-0f93I|(#P/ZbysC-wcB1S<');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
